# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.10)
# Database: mubadirat-res
# Generation Time: 2017-09-17 12:29:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_username_unique` (`username`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'Robox','admin','admin@box.agency','$2y$10$lLbMDgOGlRBM.D6tmuPAPueuaU1HwZItwsyQj0mZT1SZIVXzDEQNS',NULL,'2017-09-12 02:45:03','2017-09-12 02:45:03');

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_fr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name_en`, `name_fr`, `name_ar`, `slug`, `created_at`, `updated_at`)
VALUES
	(1,'Tunisia','Tunisie','','','2017-09-15 19:03:47','2017-09-15 19:03:47'),
	(2,'Egypt','Egypte','','','2017-09-15 19:03:47','2017-09-15 19:03:47'),
	(3,'Libya','Libye','','','2017-09-15 19:03:47','2017-09-15 19:03:47'),
	(4,'Algeria','Algérie','','','2017-09-15 19:03:47','2017-09-15 19:03:47');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2017_03_13_084400_create_admins_table',1),
	(9,'2017_09_12_015357_create_resources_table',2),
	(10,'2017_09_12_015420_create_countries_table',2),
	(11,'2017_09_12_015500_create_services_table',2),
	(12,'2017_09_12_015538_create_programs_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table programs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_fr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;

INSERT INTO `programs` (`id`, `name_en`, `name_fr`, `name_ar`, `slug`, `created_at`, `updated_at`)
VALUES
	(2,'Coworking Spaces','Espace de coworking','','',NULL,NULL),
	(3,'Inspiration','Programmes de soutien','','',NULL,NULL),
	(4,'Financing','financement','','',NULL,NULL),
	(5,'Inspiration','Programmes de soutien','','',NULL,NULL),
	(6,'Initiatives','Initiatives','','',NULL,NULL),
	(7,'Events and Conferences','Events and Conferences','','',NULL,NULL),
	(8,'Support Organizations','Support Organizations','','',NULL,NULL);

/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `title_fr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description_en` text COLLATE utf8mb4_unicode_ci,
  `description_fr` text COLLATE utf8mb4_unicode_ci,
  `description_ar` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `country_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `services` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `programs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;

INSERT INTO `resources` (`id`, `title_en`, `title_fr`, `title_ar`, `description_en`, `description_fr`, `description_ar`, `link`, `country_slug`, `services`, `programs`, `created_at`, `updated_at`)
VALUES
	(1,'Cogite Coworking Space [TUNIS]','Cogite Coworking Space [TUNIS]',NULL,'Cogite, the largest coworking space in Tunisia, offers a coworking hub for impact-driven, entrepreneurial individuals and organizations from diverse sectors. The available facilities include meeting rooms, a large event space, AV equipment, Skype boxes, high speed internet, and even a garden with a swimming pool. The space also provides a dynamic atmosphere for its members through a variety of public and private workshops and events.','Cogite est le plus grand Coworking Space en Tunisie. Cet espace de travail moderne et chaleureux offre plusieurs facilités tel que des salles de réunions, un équipement de projection, un large espace d\'événement et même un jardin avec une piscine. Assurant ainsi l’atmosphère de travail idéale.',NULL,'http://www.cogite.tn/','1','1,2,3,4,6,7','2','2017-09-15 19:03:47','2017-09-16 01:35:56'),
	(2,'Centre Culturel Cirta [EL KEF]','Centre Culturel Cirta [EL KEF]',NULL,'The Centre Culturel Cirta in El Kef is a space  for youth to innovate and to express themselves through entrepreneurial and cultural activities. The center includes three spaces: an arts Café, a movie theater, and a coworking space with a library. The center also offers a variety of cultural events such as photography expositions, workshops and film screenings.','Le centre culturel Cirta situé au Kef est un véritable sanctuaire pour les jeunes qui veulent s’immerger dans le monde de l’art et de la culture. Le centre inclut trois grands espaces, à savoir le \"Café des Arts\", la \"salle de cinéma Cirta\" et la salle de lecture et de coworking, l\'espace culturel Cirta se veut participatif entre créateurs et jeunes des quartiers cherchant à s\'exprimer et à innover.',NULL,'http://cirta-culture.tn/','1','1','2','2017-09-15 19:04:54','2017-09-15 19:04:54'),
	(11,'CoZi Coworking Café [DJERBA]','CoZi Coworking Café [DJERBA]',NULL,'CoZi Coworking Café provides premiere coworking facilities in an idyllic setting for local and out of town visitors. CoZi features an open coworking space, along with an upstairs Café and unbeatable terrace view of the Mediterranean Sea. The space also serves as a training space for many community associations and NGOs. CoZi offers flexible, affordable pricing, accessible activities, and top notch facilities for young people, entrepreneurs, and visitors looking for a place to work in Djerba.','CoZi Coworking Café est situé à Djerba. Cet espace offre aux jeunes qui ont la volonté de prendre leur destin en main un endroit où l\'innovation et la volonté d’offrir sont au rendez-vous. Incluant aussi un Café avec une vue sur la méditerranée. Cozi est aussi l\'endroit idéal pour les entrepreneurs ainsi que toute personne passant par Djerba et cherchant un lieu où travailler, le tout à des prix très abordable.',NULL,'http://www.cozi.tn/','2','1,2,3,4,6,7,15','2','2017-09-16 01:38:45','2017-09-16 01:38:45'),
	(12,'Lingare Mahdia [MAHDIA]','Lingare Mahdia [MAHDIA]',NULL,'Lingare Mahdia is a coworking space that acts as a catalyst for sustainable change and social innovation. It offers a collaborative working space for entrepreneurial individuals and organizations along with relevant and high quality consulting and training services for its community.','Lingare Mahdia est un espace cofondé par l\'Association Pensée Nationale Libre et l\'association Cool Hisse. Cet espace de coworking oeuvre pour créer un changement accès sur l’innovation et la création de projets à fort impact sociétal. Lingare offre aussi des services de domiciliation, de mutualisation et d’accompagnement pour les associations et les entrepreneurs.',NULL,'https://www.facebook.com/lingare.mahdia/','1','1,2,3,5,7','2','2017-09-16 01:39:25','2017-09-16 01:41:05'),
	(13,'Creativa [TUNIS]','Creativa [TUNIS]',NULL,'Creativa is a coworking space located in La Marsa and targeted towards architects, interior designers, and creative professions. The center offers private offices, meeting rooms, domiciliation, and a large coworking space.','Creativa est un espace de coworking situé à La Marsa destiné aux architectes, aux designers ainsi qu\'à toutes autres professions créatives. Le centre offre une variété d\'installation à ses clients tels que des bureaux privés, des salles de réunion, un grand espace de travail ainsi qu’une équipe énergique et dévouée.',NULL,'http://creativaspace.com/','1','1,2,4,5,15','2','2017-09-16 01:40:47','2017-09-16 01:40:47'),
	(14,'Banque International Arabes de Tunis','Banque International Arabes de Tunis',NULL,'BIAT is the biggest commercial bank in Tunisia. In 2014, BIAT launched  a program called  La Fondation BIAT which works to support young Tunisian entrepreneurs.','La BIAT, banque universelle, a développé toutes les activités de banque et constitue un groupe bancaire avec ses filiales dans les domaines de l’assurance, de la gestion d’actifs, du capital-investissement ou de l’intermédiation boursière',NULL,'http://www.Biat.com.tn','1','3,4,8,11','4','2017-09-16 01:42:53','2017-09-16 01:42:53'),
	(15,'Al Baraka','Al Baraka',NULL,'Al Baraka Bank Tunisia is the first financial institution in Tunis and the Maghreb to offer services according to Islamic principles. It offers a large variety of investment products, and financial and banking services.','AL Baraka Bank offre une large panoplie de produits d’investissement et de finance et de services bancaires conformes aux principes de la finance islamique.',NULL,'http://www.ALBARAKABANK.com.tn','1','10,11,12','4','2017-09-16 01:43:38','2017-09-16 01:43:38'),
	(16,'AMEN BANK','AMEN BANK',NULL,'AMEN BANK is a Tunisian commercial bank that offers a variety of financial and banking services and products to help entrepreneurs implement development strategies and sustain their projects.','AMEN BANK propose à ses clients une gamme complète de produits de placements pour gérer les excédents de trésorerie et les formules de financement spécifiques, afin d’aider les entrepreneurs à réussir leurs stratégies de développement et pérenniser leurs projets.',NULL,'http://www.amenbank.com.tn/','1','10,11,12','4','2017-09-16 01:45:08','2017-09-16 01:45:08'),
	(17,'Arab Banking Corporation','Arab Banking Corporation',NULL,'Bank ABC Tunisie is keen to offer its expertise in trade finance, project finance, treasury and foreign exchange while actively participating in the development of the Tunisian economy.','La banque ABC opère dans le but de contribuer au développement économique de la Tunisie. La banque offre à ses clients une variété de services et de produits financiers et bancaires.',NULL,'https://www.bank-abc.com/En/Pages/default.aspx','1','10,11,12','4','2017-09-16 01:45:48','2017-09-16 01:45:48'),
	(18,'ApéroEntrepreneur','ApéroEntrepreneur',NULL,'ApéroEntrepreneur is held every first Thursday of the month for entrepreneurs to learn about new trends and to connect members of the entrepreneurship community.','ApéroEntrepreneurs est un événement organisé chaque premier jeudi du mois comme une occasion pour les entrepreneurs d\'élargir leurs réseau.',NULL,'http://tunis.drinkentrepreneurs.org','1','7','5','2017-09-16 01:49:05','2017-09-16 01:49:05');

/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_fr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;

INSERT INTO `services` (`id`, `name_en`, `name_fr`, `name_ar`, `slug`, `created_at`, `updated_at`)
VALUES
	(1,'Coworking','Espace de coworking','','',NULL,NULL),
	(2,'Training','Formation','','',NULL,NULL),
	(3,'Mentorship','Mentorat','','',NULL,NULL),
	(4,'Events','Événements','','',NULL,NULL),
	(5,'Domiciliation','Domiciliation','','',NULL,NULL),
	(6,'Café','Café','','',NULL,NULL),
	(7,'Networking','Réseau','','',NULL,NULL),
	(8,'Incubation','Incubation','','',NULL,NULL),
	(9,'Acceleration','Accélération','','',NULL,NULL),
	(10,'Investment','investissement','','',NULL,NULL),
	(11,'Loans','Prêts','','',NULL,NULL),
	(12,'Finance','financement','','',NULL,NULL),
	(13,'Microfinance','microfinance','','',NULL,NULL),
	(14,'Crowdfunding','financement participatif','','',NULL,NULL),
	(15,'Art/Culture','Art/Culture','','',NULL,NULL);

/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
