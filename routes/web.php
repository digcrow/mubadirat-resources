<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/', 'AppController@index');
Route::get('/json/{countryID?}/{filterType?}/{tagID?}', 'AppController@generateResources');
Route::get('/test', 'AppController@generateResources');

//Route::get('social/login/redirect/{provider}', ['uses' => 'AppController@redirectToProvider', 'as' => 'social.login']);
//Route::get('social/login/{provider}', 'AppController@handleProviderCallback');
// Route::post('auth/facebookLogin/{offline?}', 'AppController@facebookLogin');