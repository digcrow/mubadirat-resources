<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth:admin'],function () {

    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::resource('resources', 'ResourcesController');
    Route::resource('countries', 'CountryController');
    Route::resource('programs', 'ProgramController');
    Route::resource('services', 'ServiceController');

});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');

Route::post('login', 'Auth\LoginController@login')->name('admin.login.submit');

Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');