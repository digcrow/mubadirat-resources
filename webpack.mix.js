const mix = require('laravel-mix');
require('dotenv').load();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// https://browsersync.io/docs/options/
mix
    .browserSync({
        proxy: process.env.APP_URL
    })
    .js([
        'resources/assets/js/Frontend/facebookUtils.js',
        'resources/assets/js/Frontend/app.js'
        ],'public/js/app.js')
    .js('resources/assets/js/Backend/app.js','public/js/app-admin.js')
    .js('resources/assets/js/Backend/modules.js','public/js')
    .sass('resources/assets/sass/Frontend/app.scss', 'public/css/app.css')
    .sass('resources/assets/sass/Backend/style.scss', 'public/css/style-admin.css');
    //.version();
