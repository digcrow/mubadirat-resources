<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['id','name_en','name_ar','name_fr','slug'];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
