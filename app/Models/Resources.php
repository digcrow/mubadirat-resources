<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
    protected $fillable = ['id','title_en','title_ar','title_fr','description_en','description_ar','description_fr','link'];
    protected $hidden = [
        'created_at', 'programs','services','updated_at','country_slug'
    ];
}
