<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;

use App\Models\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $programs = Program::all();
        return view('admin.programs.index')->with([
            'programs' => $programs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $program = new Program();


        $program->name_en = $request->name_en;
        $program->name_fr = $request->name_fr;
        $program->name_ar = $request->name_ar;
        $program->slug = $request->slug;

        if($program->save()){
            return $this->index();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $program = Program::Where('id', $id)->first();

        return view('admin.programs.edit')->with([
            'program' => $program,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $program = Program::find($id);

        $program->name_en = $request->name_en;
        $program->name_fr = $request->name_fr;
        $program->name_ar = $request->name_ar;
        $program->slug = $request->slug;

        if($program->save()){
            return $this->index();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $program = Program::find($id);
        $program->delete();
        return back();

    }
}
