<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;


class LoginController extends Controller
{
    //Where to redirect admin after login.
    protected $redirectTo = '/admin';

    //Trait
    use AuthenticatesUsers;

    
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['login','logout']]);
    }
    
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }
    
    public function login(Request $request)
    {
        
        $this->validate($request, [
            //'email'=>'required|email',
            'username' => 'required',
            'password'=>'required|min:6'
        ]);
        
        if (auth()->guard('admin')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')], $request->input('remember'))) {
            return redirect()->intended(route('admin.dashboard'));
        }
        
        return redirect()->back()->withInput($request->only('username', 'remember'));
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->route( 'admin.login');
    }

}
