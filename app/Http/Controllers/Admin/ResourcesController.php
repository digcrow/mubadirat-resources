<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;

use App\Models\Resources;
use App\Models\Country;
use App\Models\Program;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ResourcesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $resources = Resources::all();
        return view('admin.resources.index')->with([
            'resources' => $resources
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.resources.create')->with([
            'programs' => Program::all(),
            'services' => Service::all(),
            'countries' => Country::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resource = new Resources();

        $services = "";

        if (!empty($request->services)) {
            $services = implode(",", $request->get('services'));
        }

        $programs = "";

        if (!empty($request->programs)) {
            $programs = implode(",", $request->get('programs'));
        }

        $resource->title_en = $request->title_en;
        $resource->description_en = $request->description_en;
        $resource->title_fr = $request->title_fr;
        $resource->description_fr = $request->description_fr;
        $resource->title_ar = $request->title_ar;
        $resource->description_ar = $request->description_ar;
        $resource->link = $request->link;
        $resource->country_slug = $request->country_slug;
        $resource->services = $services;
        $resource->programs = $programs;

        if ($resource->save()) {
            return $this->index();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = Resources::Where('id', $id)->first();
        $resource->services = explode(',', $resource->services);
        $resource->programs = explode(',', $resource->programs);

        //dd ($resource);
        return view('admin.resources.edit')->with([
            'resource' => $resource,
            'programs' => Program::all(),
            'services' => Service::all(),
            'countries' => Country::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $resource = Resources::find($id);

        $services = "";

        if (!empty($request->services)) {
            $services = implode(",", $request->get('services'));
        }

        $programs = "";

        if (!empty($request->programs)) {
            $programs = implode(",", $request->get('programs'));
        }

        $resource->title_en = $request->title_en;
        $resource->description_en = $request->description_en;
        $resource->title_fr = $request->title_fr;
        $resource->description_fr = $request->description_fr;
        $resource->title_ar = $request->title_ar;
        $resource->description_ar = $request->description_ar;
        $resource->link = $request->link;
        $resource->country_slug = $request->country_slug;
        $resource->services = $services;
        $resource->programs = $programs;

        if ($resource->save()) {
            return $this->index();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $resource = Resources::find($id);
        $resource->delete();
        return back();

    }
}
