<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $countries = Country::all();
        return view('admin.countries.index')->with([
            'countries' => $countries
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = new Country();


        $country->name_en = $request->name_en;
        $country->name_fr = $request->name_fr;
        $country->name_ar = $request->name_ar;
        $country->slug = $request->slug;

        if ($country->save()) {
            return $this->index();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::Where('id', $id)->first();

        return view('admin.countries.edit')->with([
            'country' => $country,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $country = Country::find($id);

        $country->name_en = $request->name_en;
        $country->name_fr = $request->name_fr;
        $country->name_ar = $request->name_ar;
        $country->slug = $request->slug;

        if ($country->save()) {
            return $this->index();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country $country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $country = Country::find($id);
        $country->delete();
        return back();

    }
}
