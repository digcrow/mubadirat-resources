<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\Resources;
use App\Models\Country;
use App\Models\Service;
use Illuminate\Http\Request;

use Socialite;
use Facebook\Facebook;

class AppController extends Controller
{

    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    public function index()
    {
        return view('index');
    }


    // $countryID = Integer
    // $filterType = 'program' or 'service'
    // $tagID = Integer
    //public function generateResources($countryID, $filterType, $tagID )
    public function generateResources(Request $request)
    {
        $countryID = request()->segment(2);
        $filterType = request()->segment(3);
        $tagID = request()->segment(4);


        if( $countryID == NULL ){
            $countries = Country::all();
            return $countries->toJson() ;
        }
        if( $filterType == 1 && $tagID == null ){
            $filter = Program::all();
            return $filter->toJson() ;
        }

        if( $filterType == 2  && $tagID == null ){
            $filter = Service::all();
            return $filter->toJson() ;
        }

        if($filterType == 2) {
            $resources = Resources::where([
                ['country_slug', $countryID],
                ['services','LIKE', "%".$tagID."%"],
            ])->get();
        }else{
            $resources = Resources::where([
                ['country_slug', $countryID],
                ['programs','LIKE', "%".$tagID."%"],
            ])->get();
        }

        return $resources->toJson() ;
    }


    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {

        //$user = Socialite::driver($provider)->user();


        // OAuth Two Providers
        //$token = $user->token;
        //$refreshToken = $user->refreshToken; // not always provided
        //$expiresIn = $user->expiresIn;

        // OAuth One Providers
        //$token = $user->token;
        //$tokenSecret = $user->tokenSecret;

        // All Providers
        //$user->getId();
        //$user->getNickname();
        //$user->getName();
        //$user->getEmail();
        //$user->getAvatar();


        //Retrieving User Details From Token
        //$user = Socialite::driver('github')->userFromToken($token);


    }

    public function facebookLogin(Request $request, $offline = null)
    {

        if ($offline) {

            $facebook_id = mt_rand(100000000000000, 99999999999999999);
            $name = 'Offline User';

        } else {

            $fb = new \Facebook\Facebook([
                'app_id' => config('services.facebook.client_id'),
                'app_secret' => config('services.facebook.client_secret'),
                'default_graph_version' => 'v2.5',
                'default_access_token' => $request->input('access_token'),
            ]);

            // Use one of the helper classes to get a Facebook\Authentication\AccessToken entity.
            //   $helper = $fb->getRedirectLoginHelper();
            //   $helper = $fb->getJavaScriptHelper();
            //   $helper = $fb->getCanvasHelper();
            //   $helper = $fb->getPageTabHelper();

            try {
                // Get the \Facebook\GraphNodes\GraphUser object for the current user.
                // If you provided a 'default_access_token', the '{access-token}' is optional.
                $response = $fb->get('/me');
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            //$me = $response->getGraphUser();
            //echo 'Logged in as ' . $me->getName();

            $user = $response->getGraphUser();
            $namesArray = explode(' ', $user->getName());
            $facebook_id = $user->getId();
            $name = $namesArray[0];
            $surname = $namesArray[1];
            $email = $user->getEmail();
        }

        //$participant = Participant::where('facebookID', $facebook_id)->first();

        //var_dump($participant);

    }

}
