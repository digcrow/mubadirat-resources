<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        Admin::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'email' => 'admin@mubadirat-resources.com',
            'password' => bcrypt('admin@2017')
        ]);
    }
}
