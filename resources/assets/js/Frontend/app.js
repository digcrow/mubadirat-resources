(function ($) {
    "use strict";

    // window._ = require('lodash');

    window.$ = window.jQuery = require('jquery');

    // require('bootstrap-sass');

    window.axios = require('axios');

    window.d3 = require('d3');


    if (APP_ENV == 'production') {
        console.log = function () {};
    }

    function showLoader() {
        $('.loader').removeClass('hide');
        $('body').css('overflow', 'hidden');
    }

    function hideLoader() {
        $('.loader').addClass('hide');
        $('body').css('overflow', 'visible');
    }


    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     },
    //     error: function (jqXHR) {
    //         hideLoader();
    //         if (/<html>/i.test(jqXHR.responseText))
    //             sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');
    //         else
    //             sweetAlert({
    //                 title: "Oups!",
    //                 text: jqXHR.responseText,
    //                 type: "error"
    //             });
    //     }
    // });


    var ACTIVE_LANG = window.location.search.substring(1) || 'en',
        loadingRessources = false,
        timer = null,
        fill = d3.scale.category20c(),
        width = $('.col-7').width(),
        offsetLeft = $('.col-7').offset().left,
        height = 500,
        svg = d3.selectAll('svg')
            .attr('width', width)
            .attr('height', height),
        files = ['/json','filter1.json',['programs.json','services.json'],'files.json'],
        currentFilter = 0, // 0, 1, 2 [0,1], 2
        filter1= '',
        filterIndex= -1,
        filter2= '',
        currentCountry = null,
        nodes = [],
        labels = [],
        counter = 0,
        tooltipDivID = $('<div id="messageToolTipDiv"></div>');

    var lang = {
        'fr' : {
            'res' : 'Ressources',
            'des' : 'Voulez-vous démarrer votre propre entreprise? Ou pour en savoir plus sur l\'entrepreneuriat?',
            'click' : 'Cliquez sur les cercles pour explorer',
            'prev' : '< Précédent',
            'empty':  'Pas de Ressources pour le moment',
            'link':  'En Savoir Plus'
        },
        'en' : {
            'res' : 'Resources',
            'des' : 'Want to start your own business? Or to learn more about being an entrepreneur?',
            'click' : 'Click on the circles to explore',
            'prev' : '< Previous',
            'empty' : 'No resources available',
            'link' : 'Learn More'
        },
        'ar' : {
            'res' : 'موارد',
            'des' : 'تريد أن تبدأ مشروع الخاصة بك؟ أو لمعرفة المزيد عن الأعمال؟',
            'click' : 'اضغط على الدوائر لاستكشاف',
            'prev' : '< سابق',
            'empty' : 'لا تتوفر موارد',
            'link' : 'تعلم المزيد'
        }
    };

    $('.trans.res').html(lang[ACTIVE_LANG].res);
    $('.trans.click').html(lang[ACTIVE_LANG].click);
    $('.trans.des').html(lang[ACTIVE_LANG].des);
    if(ACTIVE_LANG === 'ar'){
        $('body, .trans').attr('style', 'font-family: tahoma !important');
    }

    $('body').append(tooltipDivID);

    getWebCircle(files[currentFilter]);

    function getWebCircle(file){
        nodes = [];
        labels = [];
        counter = 0;

        loadingRessources = true;
        $('body').addClass('loading');

        d3.json(file, function(error, root) {
            $('.loader').addClass('hide');
            if(error) console.log(error);

            if(root.length === 0){
               $('.col-7').append('<div class="no-data""><h2>'+lang[ACTIVE_LANG].empty+' </h2> <a href="#" class="btn filter-btn"> '+lang[ACTIVE_LANG].prev+' </a> </div>')
            }
            var biggerCircle = 0;
            if( currentFilter == 0 || currentFilter == 1 ){
                biggerCircle = 25;
            }

            var gravity = 0.0;

            if(root.length > 13){
                gravity = 0.15;
            }
            if(root.length > 16){
                gravity = 0.2;
            }


            var force = d3.layout.force()
                .nodes(nodes)
                .links([])
                .charge(-1200)
                .chargeDistance(-1010)
                .gravity(gravity)
                .friction(0.8)
                .size([width, height])
                .on('tick', tick);

            var node = svg.selectAll('g');

            function tick(e) {
                var k = .1 * e.alpha;
                // Push nodes toward their designated focus.
                nodes.forEach(function(o, i) {
                    o.y += ( (height /2.5)- o.y) * k;
                    o.x += ((width/2.5) - o.x) * k;
                });
                node.attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });
            }

            var data = root;
            timer = setInterval(function(){

                if (nodes.length > data.length-1) {
                    $('body').removeClass('loading');
                    clearInterval(timer);
                    loadingRessources = false;
                    return;
                }

                var item = data[counter],
                    indexArray = (currentFilter !== 3 ) ? "name_"+ACTIVE_LANG : "title_"+ACTIVE_LANG;
                nodes.push({id: counter, r: ( 35 + (item[indexArray].length *1.2) )+ biggerCircle, item: item});

                force.start();

                node = node.data(nodes);

                var n = node.enter().append('g')
                    .attr('class', 'node')
                    .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
                    .style('cursor', 'pointer')
                    .attr('data-sub-cat', function(d) { return d.id })
                    .on('mouseover', function(d,i) {
                        var tooltipText =  d.item['name_'+ACTIVE_LANG];
                        hoverEffect(i);
                        showToolTip( tooltipText ,d.x+d3.mouse(this)[0]+25+offsetLeft,d.y+d3.mouse(this)[1]+70,true);
                    })
                    .on('mousemove', function(d,i) {
                        tooltipDivID.css({top:d.y+d3.mouse(this)[1]+70,left:d.x+d3.mouse(this)[0]+25+offsetLeft});
                    })
                    .on('mouseout', function(){
                        showToolTip('',0,0,false);
                        resetHoverEffect();
                    })
                    .on('mousedown', getNextCat)
                    .call(force.drag);

                n.append('circle')
                    .attr('r',  function(d) { return d.r; })
                    .attr('id', function(d,i) { return 'item-'+ i ;  })
                    .style('fill', function() { return 'hsl(' + Math.random() * 360 + ',30%,70%)';});

                n.append('clipPath')
                    .attr('id', function(d,i) { return 'clip-' + i ; })
                    .append('use')
                    .attr('xlink:href', function(d,i) { return '#item-' + i ; });

                n.append('text')
                    .attr('clip-path', function(d,i) { return 'url(#clip-' + i+ ')'; })
                    .selectAll('tspan')
                    .data(function(d) {
                        return d.item[indexArray].split(' ');
                    })
                    .enter().append('tspan')
                    .attr('x', 0)
                    .attr('text-anchor', 'middle')
                    .attr('y', function(d, i,nodes) {
                        var dy0 = -(n.selectAll('tspan')[0].length - 1.5) / 2;
                        return (dy0 + i) + 'em';
                    })
                    .text(function(d) { return d; })
                    .attr('dy', '0.1em');

                if( currentFilter === 3 ){
                    n.append('desc')
                        .html(function(d,i) {
                            return '<h3>'+ d.item['title_'+ACTIVE_LANG]+ '</h3>'
                                + '<p>'+d.item['description_'+ACTIVE_LANG]+'</p>'
                                + '<a href="'+d.item.link+'" title="Visit Website" class="link" target="_blank"> '+lang[ACTIVE_LANG].link+' </a>';
                        })
                }

                counter++;
            }, 100);

            d3.selection.prototype.moveToFront = function() {
                // return this.each(function(){
                //     this.parentNode.appendChild(this);
                // });
            };

            // function resize() {
            //     width = window.innerWidth;
            //     force.size([width, height]);
            //     force.start();
            // }
            // d3.select(window).on('resize', resize);

        });

        function getNextCat(d,i) {

            if(loadingRessources) return;
            $('.loader').removeClass('hide');

            var URI = '';

            if(currentFilter !== 3) {
                svg.selectAll('g').classed('kill', true);

                if (currentFilter === 0) {
                    currentCountry = d.item.id;
                    URI += '/'+currentCountry;
                    setTimeout(function() {
                        currentFilter++;
                        svg.selectAll('*').remove();
                        getWebCircle(files[1]);
                    }, 400);
                    $('.history-wrapper').html(' <a href="#" class="btn filter-btn"> '+lang[ACTIVE_LANG].prev+' </a> ');
                    return;
                }
                if (currentFilter === 1) {
                    currentFilter++;
                    filter1 = d.item.id;
                    filterIndex = i;
                    URI += '/'+currentCountry+'/'+filter1;
                    setTimeout( function(){
                        svg.selectAll('*').remove();
                        getWebCircle(files[0]+ URI );
                    }, 400);
                    return;
                }
                if (currentFilter === 2) {
                    currentFilter++;
                    filter2 = d.item.id;
                    URI += '/'+currentCountry+'/'+filter1+'/'+filter2;
                    setTimeout( function(){
                        svg.selectAll('*').remove();
                        getWebCircle(files[0]+ URI );
                    }, 400);
                    return;
                }
            }
            $('.loader').addClass('hide');
            $('#descriptionContainer').html($(this).find('desc').html());
            d3.select(this).moveToFront();
        }

        function hoverEffect(i){
            svg.selectAll('circle')
                .classed('hovered', function(d,cI){
                    if(i === cI ) return true
                })
                .classed('not-hovered', function(d,cI){
                    if(i !== cI ) return true
                });
        }
        function resetHoverEffect(){
            svg.selectAll('circle')
                .classed('not-hovered', false)
                .classed('hovered', false);
        }
    }

    $('.history-wrapper, .col-7').on('click','a', function(){
        if(currentFilter === 0 ){
            return ;
        }
        if($('.no-data').length>0){
            $('.no-data').remove();
        }

        if(loadingRessources){
            return
        }
        $('.loader').removeClass('hide');
        var URI = '';

        currentFilter--;
        svg.selectAll('g').classed('kill', true);

        if(currentFilter === 1){
            URI += '/'+currentCountry;
            setTimeout(function() {
                svg.selectAll('*').remove();
                getWebCircle(files[1]);
            }, 100);
            return
        }

        if(currentFilter === 2){
            URI += '/'+currentCountry+'/'+filter1 ;
            setTimeout(function(){
                svg.selectAll('*').remove();
                getWebCircle(files[0]+URI)
            }, 100);
            return
        };
        if(currentFilter == 0){
            $('.history-wrapper').html('');
        }

        setTimeout(function() {
            svg.selectAll('*').remove();
            getWebCircle(files[0]+URI)
        }, 100)

    });

    function showToolTip(pMessage,pX,pY,pShow) {
        if (!pShow) { tooltipDivID.hide(); return;}
        tooltipDivID.html(pMessage);
        tooltipDivID.css({top:pY,left:pX});
        tooltipDivID.show();
    }

})(jQuery);