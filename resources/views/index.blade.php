@extends('layout')

@push('stylesheets')
<style></style>
@endpush

@section('content')
    <div class="container">
        <div class="col-7">
            <svg></svg>
        </div>
        <div class="col-3">
            <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-bottom gdlr-core-item-pdlr"
                 style="padding-bottom: 10px;">
                <div class="gdlr-core-title-item-title-wrap"><h3
                            class="gdlr-core-title-item-title gdlr-core-skin-title"
                            style="font-size: 58px;font-weight: 600;letter-spacing: 0px;text-transform: none; font-family: 'Montserrat', sans-serif; color:#20989e"><span
                                class="gdlr-core-title-item-side-border gdlr-core-skin-divider gdlr-core-left"
                                style="border-color: #2ab9da;"></span><span class="trans res">Resources</span></h3></div>
                <span class="gdlr-core-title-item-caption gdlr-core-info-font gdlr-core-skin-caption trans des" style="font-size: 16px">
                        Want to start your own business? Or to learn more about being an entrepreneur?</span>
            </div>
            <div class="history-wrapper"></div>
            <div id="descriptionContainer">
                <h3><i class="glyphicon glyphicon-chevron-left"></i> <span class="trans click">Click on the circles to explore</span></h3>
                <img src="{{ asset('images/nodes.png') }}" alt="" width="100px">
            </div>
        </div>
    </div>
@endsection

@push('scripts')
{{--<script type="text/javascript"></script>--}}
@endpush