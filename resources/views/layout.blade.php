<!doctype html>
<!--[if lt IE 9]>
<html class="no-js lt-ie9" lang="{{ config('app.locale') }}">
<![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ config('app.locale') }}">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon-152.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32.png">

    <title>@yield('title'){{ Config::get('app.name') }}</title>

    @include('partials.socialMetaTags', [
        'title' => 'Title of the page',
        'description' => 'Description of the page'
    ])

    <meta name="author" content="Rami Jegham"/>

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link href="https://fonts.googleapis.com/css?family=Asap:400,700|Montserrat:700" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        APP_ENV = '{{ app()->environment() }}';
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="@yield('class')">

<!--[if lt IE 9]>
<div class="alert alert-dismissible outdated-browser show" role="alert">
    <h6>Votre navigateur est obsolète !</h6>
    <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à
        jour votre navigateur.
        <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
    </p>
    <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
</div>
<![endif]-->

<div class="loader">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

{{--<header class="header" data-navigation-offset="75px" style="display: block;">--}}
    {{--<div class="container">--}}
        {{--<!-- Brand and toggle get grouped for better mobile display -->--}}
        {{--<div class="navbar-header">--}}
            {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">--}}
                {{--<span class="sr-only">Toggle navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
            {{--</button>--}}
            {{--<a class="navbar-brand" href="#"><img src="{{ asset('images/logo.png') }}" alt=""></a>--}}
        {{--</div>--}}

        {{--<!-- Collect the nav links, forms, and other content for toggling -->--}}
        {{--<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/home" data-anchor="#home">Home</a></li>--}}
                {{--<li class="menu-item active"><a href="#" data-anchor="#resources">Resources</a></li>--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/#what-is-mubadirat" data-anchor="#what-is-mubadirat">About Us</a></li>--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/#profiles" data-anchor="#profiles">Profiles</a></li>--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/#testimonies" data-anchor="#testimonies">Testimonies</a></li>--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/#media-mentions" data-anchor="#media-mentions">Media Mentions</a></li>--}}
                {{--<li class="menu-item"><a href="http://mubadirat.com/#partners" data-anchor="#partners">Partners</a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</header>--}}

<main class="main-wrapper">

    @yield('content')

</main>

<script src="{{ mix('js/app.js') }}"></script>

@stack('scripts')

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', '{{ env('GOOGLE_ANALYTICS_ID') }}', 'auto');
    ga('send', 'pageview');
</script>

</body>
</html>