@extends('admin.app')

@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" target="_blank">Website</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <!-- ./Breadcrumb -->

    <!-- container-fluid -->
    <div class="container-fluid">

        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Add New </strong>Resource
                        </div>
                        <div class="card-block">
                            <form action="{{ action('Admin\ServiceController@update', $service['id']) }}"
                                  method="post" class="form-horizontal">
                                {!! csrf_field() !!}
                                {{ method_field('PUT') }}
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_en">Name EN</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_en" name="name_en" class="form-control"
                                               placeholder="Name EN" value="{{ $service->name_en }}">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_fr">Name FR</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_fr" name="name_fr" class="form-control"
                                               placeholder="Name FR" value="{{ $service->name_fr }}">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_ar">Name AR</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_ar" name="name_ar" class="form-control"
                                               placeholder="Name AR" value="{{ $service->name_ar }}">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="slug">Slug</label>
                                    <div class="col-md-9">
                                        <input type="text" id="slug" name="slug" class="form-control"
                                               placeholder="Slug"
                                               value="{{ $service->slug }}">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-sm btn-primary"><i
                                                class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
@endsection
