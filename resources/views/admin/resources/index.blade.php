@extends('admin.app')

@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" target="_blank">Website</a></li>
        <li class="breadcrumb-item active">Liste Resources</li>
    </ol>
    <!-- ./Breadcrumb -->

    <!-- container-fluid -->
    <div class="container-fluid">


        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            List Resources
                        </div>
                        <div class="btn">
                            <a class="btn btn-success" href="{{ route('resources.create') }}"> + Add Resources </a>
                        </div>
                        <div class="card-block">
                            <table class="table table-responsive table-hover table-outline mb-0">
                                <thead class="thead-default">
                                <tr>
                                    <th class="text-center" width="">Title Anglais</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center" width="50">Country</th>
                                    <th class="text-center" width="200">link</th>
                                    <th class="text-center" width="100">Services</th>
                                    <th class="text-center" width="80">Programs</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($resources as $key => $resource)
                                    <tr>
                                        <td><h6>{{ $resource->title_en }}</h6></td>
                                        <td>{{  str_limit( $resource->description_en, $limit = 150, $end = '...' ) }}</td>
                                        <td class="text-center">{{ $resource->country_slug }}</td>
                                        <td class="text-center" width="200">{{  str_limit( $resource->link, $limit = 20, $end = '...' ) }}</td>
                                        <td class="text-center">{{ $resource->services }}</td>
                                        <td class="text-center">{{ $resource->programs }}</td>
                                        <td><a href="{{action('Admin\ResourcesController@edit', $resource['id'])}}"
                                               class="btn btn-warning">Edit</a>
                                            <hr>
                                            <form action="{{action('Admin\ResourcesController@destroy', $resource['id'])}}"
                                                  method="post">
                                                {{csrf_field()}}
                                                <label><input type="checkbox" name="_method" value="DELETE" required="true"> Confirm </label>
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>


    </div>
    <!-- /.container-fluid -->
@endsection
