@extends('admin.app')

@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" target="_blank">Website</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <!-- ./Breadcrumb -->

    <!-- container-fluid -->
    <div class="container-fluid">


        <div class="animated fadeIn">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Add New </strong>Resource
                        </div>
                        <div class="card-block col-md-8 col-md-offset-2">
                            <form action="{{ route('resources.store') }}" method="post" class="form-horizontal">
                                {!! csrf_field() !!}

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_en">Title EN</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_en" name="title_en" class="form-control"
                                               placeholder="Title EN">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_fr">Title FR</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_fr" name="title_fr" class="form-control"
                                               placeholder="Title FR">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_ar">Title AR</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_ar" name="title_ar" class="form-control"
                                               placeholder="Title AR">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="description_en">Description
                                        EN</label>
                                    <div class="col-md-9">
                                        <textarea type="text" id="description_en" name="description_en"
                                                  class="form-control" placeholder="Description EN"></textarea>
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="description_fr">Description
                                        FR</label>
                                    <div class="col-md-9">
                                        <textarea type="text" id="description_fr" name="description_fr"
                                                  class="form-control" placeholder="Description FR"></textarea>
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="description_ar">Description
                                        AR</label>
                                    <div class="col-md-9">
                                        <textarea type="text" id="description_ar" name="description_ar"
                                                  class="form-control" placeholder="Description AR"></textarea>
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="description_en">Link To File</label>
                                    <div class="col-md-9">
                                        <input type="text" id="link" name="link" class="form-control"
                                               placeholder="URL To File">
                                        <!--<span class="help-block">bla bla bla</span>-->
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="country_slug">Country</label>
                                    <div class="col-md-9">
                                        <select id="country_slug" name="country_slug" class="form-control">
                                            <option value="" disabled selected>Choose Your Country</option>
                                            @foreach ($countries as $index=>$item)
                                                <option value="{{ $item->id }}">{{ $item->name_fr}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                {{--<div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="services">Services :</label>
                                    <div class="col-md-9">
                                        <select id="services" name="services[]" class="form-control" size="10"
                                                multiple="">
                                            <option value="" disabled>Choose Your services</option>
                                            @foreach ($services as $index=>$item)
                                                <option value="{{ $item->id }}">{{ $item->name_fr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>--}}

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Check your services</label>
                                    <div class="col-md-9">
                                        @foreach ($services as $index=>$item)
                                            <label class="checkbox-inline" for="services{{ $item->id }}">
                                                <input type="checkbox" id="services{{ $item->id }}"
                                                       name="services[]" value="{{ $item->id }}"> {{ $item->name_fr }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label">Check your programs</label>
                                    <div class="col-md-9">
                                        @foreach ($programs as $index=>$item)
                                            <label class="checkbox-inline" for="programs{{ $item->id }}">
                                                <input type="checkbox" id="programs{{ $item->id }}"
                                                       name="programs[]" value="{{ $item->id }}"> {{ $item->name_fr }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-sm btn-primary"><i
                                                class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->
@endsection
