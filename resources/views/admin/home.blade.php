@extends('admin.app')

@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" target="_blank">Website</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <!-- ./Breadcrumb -->

    <!-- container-fluid -->
    <div class="container-fluid">


        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <a href="{{ route('resources.create') }}" class=" btn card card-inverse card-primary">
                        <div class="card-block pb-0">
                            <h4 class="mb-0"> Resource</h4>
                            <p>Add New</p>
                        </div>
                    </a>
                </div>
                <!--/.col-->

                <div class="col-sm-6 col-lg-3">
                    <a href="{{ route('countries.create') }}" class="btn card card-inverse card-info">
                        <div class="card-block pb-0">
                            <h4 class="mb-0">Country
                            </h4>
                            <p>Add New</p>
                        </div>
                    </a>
                </div>
                <!--/.col-->

                <div class="col-sm-6 col-lg-3">
                    <a href="{{ route('services.create') }}" class="btn card card-inverse card-warning">
                        <div class="card-block pb-0">
                            <h4 class="mb-0">Service
                            </h4>
                            <p>Add New</p>
                        </div>
                    </a>
                </div>
                <!--/.col-->

                <div class="col-sm-6 col-lg-3">
                    <a href="{{ route('programs.create') }}" class="btn card card-inverse card-danger">
                        <div class="card-block pb-0">
                            <h4 class="mb-0">Program
                            </h4>
                            <p>Add New</p>
                        </div>
                    </a>
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->

        </div>
        <!-- /.container-fluid -->
@endsection
