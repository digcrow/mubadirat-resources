@extends('admin.app')

@section('content')

    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/" target="_blank">Website</a></li>
        <li class="breadcrumb-item active">List Countries</li>
    </ol>
    <!-- ./Breadcrumb -->

    <!-- container-fluid -->
    <div class="container-fluid">


        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            List Resources
                        </div>
                        <div class="btn">
                            <a class="btn btn-success" href="{{ route('countries.create') }}"> + Add Country </a>
                        </div>
                        <div class="card-block">
                            <table class="table table-responsive table-hover table-outline mb-0">
                                <thead class="thead-default">
                                <tr>
                                    <th class="text-center">Name En</th>
                                    <th class="text-center">Name Fr</th>
                                    <th class="text-center">Name Ar</th>
                                    <th class="text-center">Slug / Code Country</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($countries as $key => $country)
                                    <tr>
                                        <td class="text-center">{{ $country->name_en }}</td>
                                        <td class="text-center">{{ $country->name_fr }}</td>
                                        <td class="text-center">{{ $country->name_ar }}</td>
                                        <td class="text-center">{{ $country->slug }}</td>
                                        <td><a href="{{action('Admin\CountryController@edit', $country['id'])}}"
                                               class="btn btn-warning">Edit</a>
                                            <hr>
                                            <form action="{{action('Admin\CountryController@destroy', $country['id'])}}"
                                                  method="post">
                                                {{csrf_field()}}
                                                <label><input type="checkbox" name="_method" value="DELETE" required="true"> Confirm </label>
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--/.col-->
            </div>
            <!--/.row-->
        </div>


    </div>
    <!-- /.container-fluid -->
@endsection
