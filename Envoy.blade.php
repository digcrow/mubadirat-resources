@servers(['web' => 'root@smart-robox.com'])

@setup
$directory = "/home/sample";
$repository = "git@bitbucket.org:boxadvertainment/robox-starter-l54.git";
@endsetup

@macro('create')
    clone
    configure
    migrate
    seed
@endmacro

@macro('deploy')
    pull
    configure
@endmacro

@macro('refresh')
    delete
    clone
    configure
    migrate
    seed
@endmacro

@task('clone')
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    composer self-update;
    composer install --prefer-dist --no-dev --no-interaction;

    cp .env.prod .env;

    echo "Project has been created";
@endtask

@task('pull')
    cd {{ $directory }};

    php artisan down;

    git pull origin master;
    composer install --prefer-dist --no-dev --no-interaction;

    cp .env.prod .env;

    php artisan up;
    echo "Deployment finished successfully!";
@endtask

@task('configure')
    cd {{ $directory }};

    php artisan config:cache;
    php artisan route:cache;
    php artisan responsecache:flush;

    chown -R www-data:www-data {{ $directory }};
    echo "Permissions have been set";

@endtask

@task('delete', ['confirm' => true])
    rm -rf {{ $directory }};
    echo "Project directory has been deleted";
@endtask

@task('reset', ['confirm' => true])
    cd {{ $directory }};
    git reset --hard HEAD;
@endtask

@task('migrate')
    cd {{ $directory }};
    php artisan migrate --force;
@endtask

@task('seed')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask